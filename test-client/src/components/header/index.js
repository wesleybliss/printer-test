import { h } from 'preact';
import { Link } from 'preact-router/match';
import style from './style';

const Header = () => (
	
    /*<header class={style.header}>
		<h1>Screenshot Test</h1>
	</header>*/
    
    <nav class="navbar is-dark" role="navigation" aria-label="main navigation">
        
        <div class="navbar-brand">
            <a class="navbar-item" href="/">
                <h1>Screenshot Test</h1>
            </a>
        </div>
        
    </nav>
    
)

export default Header
