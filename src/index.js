const fs = require('fs')
const path = require('path')
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const screenshotWeb = require('node-server-screenshot')
const Pageres = require('pageres')
const shell = require('shelljs')
const phantom = require('phantom')

const app = express()
const host = process.env.HOST || '0.0.0.0'
const port = process.env.PORT || 3000

const weasyprintBin = shell.which('weasyprint')
if (!weasyprintBin) throw new Error('Could not find weasyprint in PATH')

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

// Test client
app.use('/test-client', express.static(path.resolve(__dirname, 'test-client')))

app.get('/print', /*async*/ (req, res) => {
    
    if (!req.query.url)
        return res.status(400).send({ error: 'Must provide url' })
    
    console.info('Generating screenshot for URL:\n' + req.query.url)
    
    /*screenshotWeb.fromURL(
        req.query.url,
        'temp.png',
        function() {
            res.send(200, { success: 'Webpage saved as image' })
        }
    )*/
    
    try {
        new Pageres({ delay: 2 })
            //.src(req.query.url, ['480x320'], { crop: false })
            .src(req.query.url, ['300x483'], { crop: false })
            .dest('screenshots')
            .run()
            .then(data => {
                console.log('Sending back', data[0].filename)
                res.status(200).send({ filename: data[0].filename })
            })
            .catch(e => {
                console.error('catch fail')
                res.status(500).send({ error: e })
            })
    } catch (e) {
        console.error('trycatch fail')
        res.status(500).send({ error: e })
    }
    
    /*const tempUri = path.join(__dirname, `../temp/page.html`)
    const inputUri = req.query.url
    const outputUri = path.join(__dirname, `../screenshots/saved_${Date.now()}.pdf`)
    
    const instance = await phantom.create()
    const page = await instance.createPage()
    await page.on('onResourceRequested', requestData => {
        console.info('Requesting', requestData.url)
    })
    const status = await page.open(inputUri)
    const content = await page.property('content')
    //console.log(content)
    fs.writeFileSync(tempUri, content, 'utf8')
    await instance.exit()
    
    const args = [
        'python3',
        weasyprintBin,
        //inputUri,
        tempUri,
        outputUri
        //' -s ' + pdfStylesheet,
    ].join(' ')
    
    console.log('Executing command', args)
    
    if (shell.exec(args).code !== 0)
        res.status(500).send({ error: 'Failed to weasyprint URL' })
    else
        res.status(200).send({ filename: outputUri })*/
    
})

app.get('/preview', (req, res) => {
    
    if (!req.query.filename)
        return res.status(400).send({ error: 'Must provide filename' })
    
    const uri = path.resolve(__dirname, '../screenshots/' + req.query.filename)
    const file = fs.readFileSync(uri)
    
    res.type('png')
    res.send(file)
    
})

app.listen(port, host, () =>
    console.log(`Listening on port http://${host}:${port}`))