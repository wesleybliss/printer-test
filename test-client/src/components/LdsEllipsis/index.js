import { h } from 'preact';
import { Link } from 'preact-router/match';
import style from './style';

const LdsEllipsis = () => (
    <div class={style['lds-ellipsis']}>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
    </div>
)

export default LdsEllipsis
