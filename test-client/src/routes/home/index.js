import { h, Component } from 'preact'
import style from './style'
import LdsEllipsis from '../../components/LdsEllipsis'

class Home extends Component {
    
    state = {
        url: 'https://',
        previewUrl: '',
        previewData: '',
        loading: false,
        error: null
    }
    
    handleOnUrlChange = e =>
        this.setState({ url: e.target.value })
    
    handleGetScreenshotClick = () =>
        this.setState({
            loading: true,
            error: null
        }, () => {
            this.fetchScreenshot(this.state.url)
        })
    
    fetchScreenshot = async (url) => {
        
        /*renderScreenshot()
        return*/
        
        try {
            
            const res = await fetch(`http://localhost:3000/print?url=${url}`)
            const json = await res.json()
            
            console.log('response', json)
            
            this.setState({
                error: null,
                loading: false,
                previewUrl: `http://localhost:3000/preview?filename=${json.filename}`
            })
            
        }
        catch (e) {
            
            this.setState({
                error: e.toString(),
                loading: false,
                previewUrl: ''
            })
            
        }
        
    }
    
    render() {
        
        const {
            error,
            url,
            previewUrl,
            loading
        } = this.state
        
        return (
            
        	<div class={style.home}>
        		
                <div class="columns">
                    <div class="column">
                        <input
                            class="input"
                            type="text"
                            size="50"
                            value={url}
                            onInput={this.handleOnUrlChange} />
                    </div>
                </div>
                
                {/*<div class="columns">
                    <div class="column">
                        <pre><code>{url}</code></pre>
                    </div>
                </div>*/}
                
                <div class="columns">
                    <div class="column">
                        {loading && <LdsEllipsis />}
                        {!loading && (
                            <button
                                class="button"
                                onClick={this.handleGetScreenshotClick}>
                                Get Screenshot
                            </button>
                        )}
                    </div>
                </div>
                
                <div class="columns">
                    <div class="column">
                        <img src={previewUrl} />
                    </div>
                </div>
                
                <div class="columns hidden">
                    <div class="column">
                        <img src="" width="640" height="480" />
                        <canvas id="canvas" style="border:2px solid red;" width="200" height="200"></canvas>
                    </div>
                </div>
                
                {error && (
                    <div class="columns is-danger">
                        <div class="column">
                            <b>{error}</b>
                        </div>
                    </div>
                )}
                
        	</div>
            
        )
        
    }
    
}

export default Home
